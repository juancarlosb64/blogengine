﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BlogEngine.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace BlogEngine.Controllers
{
    [Authorize]
    [Route("Services/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private BlogContext _context;

        public PostsController(BlogContext context)
        {
            _context = context;
        }

        #region All Roles

        [Route("")]
        [HttpGet]
        public IActionResult Get()
        {
            var _data = (from d in _context.Posts
                         join u in _context.Users on d.Author equals u.User
                         where d.Publish
                         select new { d.Id, d.Title, d.Content, Author = u.Names, Status = "Published" }).ToList();

            return Ok(_data);
        }

        [Route("Published/{id}")]
        [HttpGet]
        public IActionResult GetPublished(int id)
        {
            var _row = (from d in _context.Posts
                        join u in _context.Users on d.Author equals u.User
                        where d.Publish && d.Id == id
                        select new { d.Id, 
                                     Date = d.Date.GetValueOrDefault().ToString("dd/MM/yyyy"), 
                                     d.Title, 
                                     d.Content, 
                                     Author = u.Names, 
                                     Status = "Published", 
                                     Comments = (from c in _context.Comments
                                                 join u in _context.Users on c.CreateUser equals u.User
                                                 where c.Id == d.Id
                                                 orderby c.RowId descending
                                                 select new { Date = c.CreateDate.ToString("dd/MM/yyyy hh:mm tt"), User = u.Names, c.Comment }).ToList()
                        }).FirstOrDefault();

            if (_row == null) return StatusCode(404, new { Status = 404, Title = "Not Found" });

            return Ok(_row);
        }

        [Route("Comment")]
        [HttpPost]
        public IActionResult PostComment([FromBody] Comment data)
        {
            try
            {
                var _post = _context.Posts.Include(d => d.Comments)
                                          .Where(d => d.Id == data.Id)
                                          .FirstOrDefault();
                if (_post == null) return NotFound();
                else if (!_post.Publish) return Unauthorized();

                //Comments...
                Comments _newCom = new Comments();
                _newCom.Id = data.Id;
                _newCom.Comment = data.Comments;
                _newCom.CreateDate = DateTime.Now;
                _newCom.CreateUser = HttpContext.User.Identity.Name;

                //Save data...
                //_context.Comments.Add(_comment);
                _post.Comments.Add(_newCom);
                _context.Posts.Update(_post);
                _context.SaveChanges();

                return Ok(new { Status = 200, Title = "Ok", Message = "Records Saved" });
            }
            catch
            {
                return BadRequest();
            }
        }

        #endregion

        #region Writter Role

        [Authorize(Roles = "Writer")]
        [Route("Posts")]
        [HttpGet]
        public IActionResult GetPosts()
        {
            var _data = (from p in _context.Posts
                         join l in _context.LogPosts on p.Id equals l.Id
                         join u in _context.Users on p.Author equals u.User
                         join s in _context.Status on l.StatusId equals s.StatusId
                         where l.Current && p.Author == HttpContext.User.Identity.Name
                         orderby p.Id descending
                         select new
                         {
                             p.Id,
                             p.Title,
                             p.Content,
                             Status = s.Description,
                             s.Color,
                             Edit = l.StatusId == 1 || l.StatusId == 4 ? true : false,
                             Submit = l.StatusId == 1 ? true : false
                         }).ToList();

            return Ok(_data);
        }

        [Authorize(Roles = "Writer")]
        [Route("{id}")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            var _post = _context.Posts.Include(d => d.LogPosts)
                                      .Where(d => d.Id == id && d.Author == HttpContext.User.Identity.Name)
                                      .Select(d => new { d.Id, d.Title, d.Content, Comment = d.LogPosts.Where(d => d.Current && d.StatusId == 4).FirstOrDefault().Comments })
                                      .FirstOrDefault();

            if (_post == null) return NotFound();

            return Ok(_post);
        }

        [Authorize(Roles = "Writer")]
        [Route("")]
        [HttpPost]
        public IActionResult Post([FromBody] Post data)
        {
            try
            {
                Posts _post = new Posts();
                _post.LogPosts = new List<LogPosts>();

                //Post...
                _post.Author = HttpContext.User.Identity.Name;
                _post.Title = data.Title;
                _post.Content = data.Content;
                _post.CreateDate = DateTime.Now;
                _post.ModifDate = DateTime.Now;

                //Log...
                LogPosts _newlog = new LogPosts();
                _newlog.StatusId = 1;
                _newlog.Current = true;
                _newlog.CreateDate = DateTime.Now;
                _newlog.CreateUser = HttpContext.User.Identity.Name;
                _newlog.ModifDate = DateTime.Now;
                _newlog.ModifUser = HttpContext.User.Identity.Name;
                _post.LogPosts.Add(_newlog);

                //Save data...
                _context.Posts.Add(_post);
                _context.SaveChanges();

                return Ok(new { Status = 200, Title = "Ok", Message = "Records Saved" });
            }
            catch
            {
                return BadRequest();
            }
        }

        [Authorize(Roles = "Writer")]
        [Route("")]
        [HttpPut]
        public IActionResult Put([FromBody] Post data)
        {
            try
            {
                var _post = _context.Posts.Include(d => d.LogPosts)
                                          .Where(d => d.Id == data.Id)
                                          .FirstOrDefault();
                if (_post == null) return NotFound();
                else if (_post.Publish || _post.LogPosts.Where(d => d.Current)
                                                    .Select(d => d.StatusId)
                                                    .First() == 2) return Unauthorized("Publish or Submit Post");
                else if (_post.Author != HttpContext.User.Identity.Name) return Unauthorized("Wrong Post");

                //Post...
                _post.Title = data.Title;
                _post.Content = data.Content;
                _post.ModifDate = DateTime.Now;

                //Verify status Post...
                var _log = _post.LogPosts.Where(d => d.Current).First();
                if (_log.StatusId == 4) { ChangeStatus(_post, 1); }
                else
                {
                    _context.Posts.Update(_post);
                    _context.SaveChanges();
                }

                return Ok(new { Status = 200, Title = "Ok", Message = "Records Saved" });
            }
            catch
            {
                return BadRequest();
            }
        }

        [Authorize(Roles = "Writer")]
        [Route("Submit/{id}")]
        [HttpGet]
        public IActionResult GetSubmit(int id)
        {
            try
            {
                var _post = _context.Posts.Include(d => d.LogPosts)
                                          .Where(d => d.Id == id)
                                          .FirstOrDefault();
                if (_post == null) return NotFound();
                else if (_post.Author != HttpContext.User.Identity.Name) return Unauthorized("Wrong Post");

                ChangeStatus(_post, 2);

                return Ok(new { Status = 200, Title = "Ok", Message = "Records Saved" });
            }
            catch
            {
                return BadRequest();
            }
        }

        #endregion

        #region Editor Role

        [Authorize(Roles = "Editor")]
        [Route("Pending")]
        [HttpGet]
        public IActionResult GetPending()
        {
            var _post = (from p in _context.Posts
                         join l in _context.LogPosts on p.Id equals l.Id
                         join u in _context.Users on p.Author equals u.User
                         join s in _context.Status on l.StatusId equals s.StatusId
                         where l.Current && l.StatusId == 2
                         select new { p.Id, p.Title, p.Content, Author = u.Names, Status = s.Description, Color = s.Color }).ToList();

            return Ok(_post);
        }

        [Authorize(Roles = "Editor")]
        [Route("Approve")]
        [HttpPost]
        public IActionResult PostApprove([FromBody] Process data)
        {
            try
            {
                var _post = _context.Posts.Include(d => d.LogPosts)
                                          .Where(d => d.Id == data.Id)
                                          .FirstOrDefault();
                if (_post == null) return NotFound();
                if (_post.LogPosts.Where(d => d.Current && d.StatusId != 2).Count() > 0) return Unauthorized("Wrong Post");

                ChangeStatus(_post, 3);

                return Ok(new { Status = 200, Title = "Ok", Message = "Records Saved" });
            }
            catch
            {
                return BadRequest();
            }
        }

        [Authorize(Roles = "Editor")]
        [Route("Reject")]
        [HttpPost]
        public IActionResult PostReject([FromBody] Process data)
        {
            try
            {
                var _post = _context.Posts.Include(d => d.LogPosts)
                                          .Where(d => d.Id == data.Id)
                                          .FirstOrDefault();

                if (_post == null) return NotFound();
                if(_post.LogPosts.Where(d => d.Current && d.StatusId != 2).Count() > 0) return Unauthorized("Wrong Post");

                ChangeStatus(_post, 4, data.Comments);

                return Ok(new { Status = 200, Title = "Ok", Message = "Records Saved" });
            }
            catch
            {
                return BadRequest();
            }
        }

        private void ChangeStatus(Posts post, int status, string comments = "")
        {
            //Post...
            post.ModifDate = DateTime.Now;

            //Verify status for Locked or Publish...
            if (status == 1) { post.Locked = false; }
            else if (status == 2) { post.Locked = true; }
            else if (status == 3) { post.Publish = true; post.Date = DateTime.Now; }

            //Log...
            var _logpost = post.LogPosts.Where(d => d.Current).First();
            _logpost.Current = false;
            _logpost.ModifDate = DateTime.Now;
            _logpost.ModifUser = HttpContext.User.Identity.Name;

            //New Log...
            LogPosts _newLog = new LogPosts();
            _newLog.StatusId = status;
            _newLog.Current = true;
            _newLog.CreateDate = DateTime.Now;
            _newLog.CreateUser = HttpContext.User.Identity.Name;
            _newLog.ModifDate = DateTime.Now;
            _newLog.ModifUser = HttpContext.User.Identity.Name;

            if(!string.IsNullOrEmpty(comments)) _newLog.Comments = comments;
            post.LogPosts.Add(_newLog);

            _context.Posts.Update(post);
            _context.SaveChanges();
        }

        #endregion
    }
}
