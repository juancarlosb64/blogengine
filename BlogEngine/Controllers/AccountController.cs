﻿using BlogEngine.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace BlogEngine.Controllers
{
    [AllowAnonymous]
    [Route("Services/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private BlogContext _context;
        private readonly IConfiguration _configuration;

        public AccountController(BlogContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        [Route("")]
        [HttpPost]
        public IActionResult Post([FromBody] Credential data)
        {
            try
            {
                var _user = (from u in _context.Users
                             join ur in _context.UsersRoles on u.User equals ur.User
                             join r in _context.Roles on ur.RoleId equals r.RoleId
                             where u.User == data.User
                             select new { u.Password, r.Description }).FirstOrDefault();
                
                if (_user == null) return StatusCode(200, new { Status = 404, Title = "User Not Found" });
                else if (_user.Password != data.Password) return StatusCode(200, new { Status = 401, Title = "Password Incorrect" });

                var _login = new { Status = 200, Title = "Ok", User = data.User, Token = GenerateToken(data, _user.Description) };

                return Ok(_login);
            }
            catch
            {
                return BadRequest();
            }
        }

        private string GenerateToken(Credential data, string role)
        {
            var _symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var _signingCredentials = new SigningCredentials(_symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

            //Claims...
            var _claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, data.User),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, role)
            };

            var _token = new JwtSecurityToken(
                issuer: _configuration["Jwt:Issuer"],
                audience: _configuration["Jwt:Audience"],
                claims: _claims,
                expires: DateTime.Now.AddMinutes(15),
                signingCredentials: _signingCredentials);

            return new JwtSecurityTokenHandler().WriteToken(_token);

        }
    }
}
