﻿using System.ComponentModel.DataAnnotations;

namespace BlogEngine.Models
{
    public class Credential
    {
        [Required(ErrorMessage = "{0} Required")]
        public string User { get; set; }
        [Required(ErrorMessage = "{0} Required")]
        public string Password { get; set; }
    }
}
