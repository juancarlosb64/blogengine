﻿using System.ComponentModel.DataAnnotations;

namespace BlogEngine.Models
{
    public class Post
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }
        [Required]
        [MaxLength(600)]
        public string Content { get; set; }
    }

    public class Process
    {
        [GreaterZero(ErrorMessage = "{0} Required")]
        public int Id { get; set; }
        [Required(AllowEmptyStrings = true)]
        [MaxLength(200, ErrorMessage = "{1} Max length")]
        public string Comments { get; set; }
    }

    public class Comment
    {
        [GreaterZero(ErrorMessage = "{0} Required")]
        public int Id { get; set; }
        [Required(ErrorMessage = "{0} Required")]
        [MaxLength(200, ErrorMessage = "{1} Max length")]
        public string Comments { get; set; }
    }
}
