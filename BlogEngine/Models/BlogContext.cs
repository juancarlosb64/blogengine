﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Hosting;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogEngine.Models
{
    public class BlogContext : DbContext
    {
        public DbSet<Posts> Posts { get; set; }
        public DbSet<Comments> Comments { get; set; }
        public DbSet<LogPosts> LogPosts { get; set; }
        public DbSet<Users> Users { get; set; }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<UsersRoles> UsersRoles { get; set; }
        public DbSet<Status> Status { get; set; }

        public BlogContext(DbContextOptions<BlogContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Posts>(post => {
                post.ToTable("Posts");
                post.HasKey(d => d.Id);
                post.Property(d => d.Date).HasColumnType("date");
            });

            modelBuilder.Entity<Comments>(comment => {
                comment.ToTable("Comments");
                comment.HasKey(d => d.RowId);
                comment.HasOne(d => d.Posts).WithMany(d => d.Comments).HasForeignKey(d => d.Id);
            });

            modelBuilder.Entity<Status>(status =>
            {
                status.ToTable("Status");
                status.HasKey(d => d.StatusId);
            });

            modelBuilder.Entity<LogPosts>(logPost => {
                logPost.ToTable("LOGPOSTS");
                logPost.HasKey(d => d.RowId);
                logPost.HasOne(d => d.Posts).WithMany(d => d.LogPosts).HasForeignKey(d => d.Id);
                logPost.HasOne(d => d.Status).WithMany(d => d.LogPosts).HasForeignKey(d => d.StatusId);
                logPost.Property(d => d.Comments).IsRequired(false);
            });

            modelBuilder.Entity<Users>(users =>
            {
                users.ToTable("Users");
                users.HasKey(d => d.User);
            });

            modelBuilder.Entity<Roles>(roles =>
            {
                roles.ToTable("Roles");
                roles.HasKey(d => d.RoleId);
            });

            modelBuilder.Entity<UsersRoles>(userRoles =>
            {
                userRoles.ToTable("UsersRoles");
                userRoles.HasKey(d => d.RowId);
                userRoles.HasOne(d => d.Users).WithMany(d => d.UsersRoles).HasForeignKey(d => d.User);
                userRoles.HasOne(d => d.Roles).WithMany(d => d.UsersRoles).HasForeignKey(d => d.RoleId);
            });
        }
    }

    public class Posts
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Author { get; set; }
        public Nullable<DateTime> Date { get; set; }
        public bool Publish { get; set; }
        public bool Locked { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifDate { get; set; }

        public List<Comments> Comments { get; set; }
        public List<LogPosts> LogPosts { get; set; }
    }

    public class Comments
    {
        public int RowId { get; set; }
        public string Comment { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateUser { get; set; }

        public int Id { get; set; }
        public Posts Posts { get; set; }
    }

    public class Status
    {
        public int StatusId { get; set; }
        public string Description { get; set; }
        public string Color { get; set; }

        public List<LogPosts> LogPosts { get; set; }
    }

    public class LogPosts
    {
        public int RowId { get; set; }
        public string Comments { get; set; }
        public bool Current { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateUser { get; set; }
        public DateTime ModifDate { get; set; }
        public string ModifUser { get; set; }

        public int Id { get; set; }
        public Posts Posts { get; set; }

        public int StatusId { get; set; }
        public Status Status { get; set; }
    }

    public class Users
    {
        public string User { get; set; }
        public string Names { get; set; }
        public string Password { get; set; }

        public List<UsersRoles> UsersRoles { get; set; }
    }

    public class Roles
    {
        public int RoleId { get; set; }
        public string Description { get; set; }

        public List<UsersRoles> UsersRoles { get; set; }
    }

    public class UsersRoles
    {
        public int RowId { get; set; }

        public string User { get; set; }
        public Users Users { get; set; }

        public int RoleId { get; set; }
        public Roles Roles { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class GreaterZeroAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return Convert.ToDecimal(value) > 0;
        }
    }
}
