
# Blog Engine

The BlogEngine project is built on asp.net Core 6, with EntityFramework Core as the ORM. 
BlogEngine has CORS configured and activated, also JWT which allows generating a token when 
users log in, this token has a duration of 15 minutes.

The API is authorized by roles which are stored in the database in the Roles table. The 
Swashbuckle package was installed, which allows generating the API documentation with 
Swagger UI, by default the API loads it when it is executed.

The api was tested with swagger and once JWT was configured, Postman was used to speed up the 
testing process. The database manager is sql server, attached a .rar file with the database 
backup or script.
## Configuration

To run the project you need:
- Clone with visual studio 2022 the project.
- Modify the connection string found in appsettings.json.
- Recover the backup or run the script on sql server.

## Collection

Login

```bash
POST
    https://localhost:7178/Services/Account
    {
        "user": "string",
        "password": "string"
    }
```
All Roles

```bash
GET
    https://localhost:7178/Services/Posts
    https://localhost:7178/Services/Posts/Published/{id}

POST
    https://localhost:7178/Services/Posts/Comment
    {
        "id": "int",
        "comments": "string"
    }
```
Writer

```bash
GET
    https://localhost:7178/Services/Posts/Posts
    https://localhost:7178/Services/Posts/{id}
    https://localhost:7178/Services/Posts/Submit/{id}

POST
    https://localhost:7178/Services/Posts
    {
        "id": "int",
        "title": "string",
        "content": "string"
    }
    
PUT
    https://localhost:7178/Services/Posts
    {
        "id": "int",
        "title": "string",
        "content": "string"
    }
```
Editor

```bash
GET
    https://localhost:7178/Services/Posts/Pending

POST
    https://localhost:7178/Services/Posts/Approve
    {
        "id": "int",
        "comments": "string"
    }

    https://localhost:7178/Services/Posts/Reject
    {
        "id": "int",
        "comments": "string"
    }
```
## User

All users have the same password which is: user*123

- User: ebordas, Role: Public
- User: jbordas, Role: Writer
- User: mbordas, Role: Editor

## Times

- 08/11/2022 -> 08:00 p.m. - 12:00 a.m.
- 09/11/2022 -> 09:30 a.m. - 01:20 p.m.
- 09/11/2022 -> 03:00 p.m. - 06:00 p.m.
- 09/11/2022 -> 07:00 p.m. - 08:30 p.m
